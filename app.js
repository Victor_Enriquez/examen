var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(express.static(__dirname+'/client'));
app.use(bodyParser.json());

User =require('./models/user');

app.get('/', function(req, res,next)  {
	res.send('works');
});

app.get('/api/users', function(req, res,next)  {
	User.findAll( ).then(function(user){
			res.json(user);
		});
});

	app.post('/api/users', function(req, res,next){
				var user = req.body;
				var aux = User.build(user);
				aux.save();
				res.json(user);
});

module.exports=app;

app.listen('3000');
console.log('Listen on 3000');
